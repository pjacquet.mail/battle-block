# Projet de Gabriel, Nathan, Julien, Edgar
#
# ----------------------------------
# ---------- BATTLE BLOCK ----------
# ----------------------------------
#

import pygame
import random

# Renvoie une pièce aléatoire
def random_piece():
    pieces = [
        ([[1, 1, 1, 1]], (255, 0, 0)),       # I piece (red)
        ([[1, 1], [1, 1]], (0, 255, 0)),     # O piece (green)
        ([[1, 0], [1, 0], [1, 1]], (0, 0, 255)),     # T piece (blue)
        ([[1, 0], [1, 1], [0, 1]], (255, 255, 0)),   # Z piece (yellow)
        ([[0, 1, 1], [1, 1, 0]], (255, 0, 255)),     # S piece (purple)
        ([[1, 1, 1], [0, 1, 0]], (0, 255, 255)),     # L piece (cyan)
        ([[1, 1, 1], [1, 0, 0]], (255, 165, 0))      # J piece (orange)
    ]
    return random.choice(pieces)

# Fait tourner la pièce
def rotate(piece):
    return list(zip(*reversed(piece)))

# Dessine la grille
def draw_grid(screen, grid, block_size, x_offset, y_offset, piece, piece_x, piece_y, piece_color):
    for x in range(len(grid)):
        for y in range(len(grid[0])):
            if grid[x][y] != 0:  # Vérifie si la case est occupé
                color = grid[x][y]
                pygame.draw.rect(screen, color, (x * block_size + x_offset, y * block_size + y_offset, block_size, block_size))
            pygame.draw.rect(screen, (255, 255, 255), (x * block_size + x_offset, y * block_size + y_offset, block_size, block_size), 1)
            
    # Dessine l'outline de la pièce en cours tout en bas
    bottom_y = find_bottom_position(piece, grid, piece_x, piece_y)
    for px in range(len(piece)):
        for py in range(len(piece[0])):
            if piece[px][py] == 1:
                pygame.draw.rect(screen, piece_color, ((piece_x + px) * block_size + x_offset, (bottom_y + py) * block_size + y_offset, block_size, block_size), 2)

# Vérifie et supprime les lignes pleines
def remove_full_lines(grid):
    lines_to_remove = []
    for y in range(len(grid[0])):
        if all(grid[x][y] != 0 for x in range(len(grid))):  # Vérifie si toutes les cases dans une ligne sont pleines
            lines_to_remove.append(y)
    for y in lines_to_remove:
        for x in range(len(grid)):
            del grid[x][y]
            grid[x].insert(0, 0)
    return len(lines_to_remove)

# Fusionne la pièce avec la grille
def merge_piece(piece, grid, piece_x, piece_y, color):
    for x in range(len(piece)):
        for y in range(len(piece[0])):
            if piece[x][y] == 1:
                grid[piece_x + x][piece_y + y] = color

# Vérifie si la pièce est en bas
def hit_bottom(piece, grid, piece_x, piece_y):
    next_y = piece_y + 1
    for x in range(len(piece)):
        for y in range(len(piece[0])):
            if piece[x][y] == 1:
                if next_y + y >= len(grid[0]) or grid[piece_x + x][next_y + y] != 0:  # Check if cell below is occupied
                    return True
    return False

# Vérifie les collisions de la pièce
def check_collision(piece, piece_x, piece_y, grid):
    for x in range(len(piece)):
        for y in range(len(piece[0])):
            if piece[x][y] == 1:
                grid_x = piece_x + x
                grid_y = piece_y + y
                if grid_x < 0 or grid_x >= len(grid) or grid_y >= len(grid[0]) or grid[grid_x][grid_y] != 0:
                    return True
    return False
    
def find_bottom_position(piece, grid, piece_x, piece_y):
    bottom_y = piece_y
    while not hit_bottom(piece, grid, piece_x, bottom_y):
        bottom_y += 1
    return bottom_y

    
# Vérifie si la partie est terminée (grille remplie jusqu'en haut)
def is_game_over(grid):
    return any(grid[x][0] != 0 for x in range(len(grid)))

pygame.init()

# Met en place la fenêtre
window_width = 700
window_height = 900
screen = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Tetris")

# Variables du jeu
block_size = 30
grid_width = 10
grid_height = 20

# Centrage de la grille
x_offset = (window_width - grid_width * block_size) // 2
y_offset = (window_height - grid_height * block_size) // 2

# Grille
grid = [[0] * grid_height for _ in range(grid_width)]

# Variables des pièces
piece, color = random_piece()
piece_x = grid_width // 2 - len(piece) // 2
piece_y = 0

# Compteur de points
points = 0

clock = pygame.time.Clock()
running = True
frame_counter = 0

# Boucle principale
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                if not check_collision(piece, piece_x - 1, piece_y, grid):
                    piece_x -= 1
            elif event.key == pygame.K_RIGHT:
                if not check_collision(piece, piece_x + 1, piece_y, grid):
                    piece_x += 1
            elif event.key == pygame.K_UP:
                rotated_piece = rotate(piece)
                if not check_collision(rotated_piece, piece_x, piece_y, grid):
                    piece = rotated_piece
   
   # Accélère la chute lorsque la flèche du bas est appuyé
    keys = pygame.key.get_pressed()
    if keys[pygame.K_DOWN]:
        frame_counter = 20
        

    screen.fill((100, 100, 100))
    pygame.draw.rect(screen, (50, 50, 50), (x_offset, y_offset, len(grid) * block_size, len(grid[0]) * block_size), 0)

    # Chute de la pièce
    if frame_counter == 20 and not is_game_over(grid):
        frame_counter = 0
        if not hit_bottom(piece, grid, piece_x, piece_y):
            piece_y += 1
        else:
            merge_piece(piece, grid, piece_x, piece_y, color)
            lines_cleared = remove_full_lines(grid)
            points += lines_cleared * 10
            piece, color = random_piece()
            piece_x = grid_width // 2 - len(piece) // 2
            piece_y = 0

    # Dessine la pièce
    if not is_game_over(grid):
        for x in range(len(piece)):
            for y in range(len(piece[0])):
                if piece[x][y] == 1:
                    pygame.draw.rect(screen, color, ((piece_x + x) * block_size + x_offset, (piece_y + y) * block_size + y_offset, block_size, block_size))

    # Dessine la grille
    draw_grid(screen, grid, block_size, x_offset, y_offset, piece, piece_x, piece_y, color)
    
    # Vérifie si la partie est terminée
    if is_game_over(grid):
        # Affichage de l'ombre
        pygame.draw.rect(screen, (0, 0, 0), (window_width // 2 - 250, window_height // 2 - 50, 500, 100), 0)
        
        # Affichage texte "Partie Terminée !"
        font = pygame.font.Font(None, 70)
        text = font.render("Partie terminée !", True, (255, 255, 255))
        text_rect = text.get_rect(center=(window_width // 2, window_height // 2))
        screen.blit(text, text_rect)
    
        pygame.display.flip()

    # Affichage du compteur de points
    font = pygame.font.Font(None, 36)
    text = font.render(f"Points: {points}", True, (255, 255, 255))
    screen.blit(text, (10, 10))

    pygame.display.flip()
    clock.tick(20)
    frame_counter += 1

pygame.quit()
